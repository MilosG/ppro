CREATE DATABASE IF NOT EXISTS taskmanager;

ALTER DATABASE taskmanager
  DEFAULT CHARACTER SET utf8
  DEFAULT COLLATE utf8_general_ci;

GRANT ALL PRIVILEGES ON taskmanager.* TO pc@localhost IDENTIFIED BY 'pc';

USE taskmanager;

CREATE TABLE IF NOT EXISTS users (
  id INT(4) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  first_name VARCHAR(30),
  last_name VARCHAR(30),
  password VARCHAR(30),
  email VARCHAR(30)
) engine=InnoDB;

CREATE TABLE IF NOT EXISTS projects (
  id INT(4) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(80),
  created_date DATE NOT NULL,
  finished_date DATE,
  project_manager_id INT(4) UNSIGNED NOT NULL,

  FOREIGN KEY (project_manager_id) REFERENCES users(id)
) engine=InnoDB;

CREATE TABLE IF NOT EXISTS users_projects (
  user_id INT(4) UNSIGNED NOT NULL,
  project_id INT(4) UNSIGNED NOT NULL,
  FOREIGN KEY (user_id) REFERENCES users(id),
  FOREIGN KEY (project_id) REFERENCES projects(id),
  UNIQUE (user_id,project_id)
) engine=InnoDB;

CREATE TABLE IF NOT EXISTS task_states (
  id INT(4) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(80),
  INDEX(name)
) engine=InnoDB;

CREATE TABLE IF NOT EXISTS task_types (
  id INT(4) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(80),
  INDEX(name)
) engine=InnoDB;

CREATE TABLE IF NOT EXISTS task_priorities (
  id INT(4) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(80),
  INDEX(name)
) engine=InnoDB;

CREATE TABLE IF NOT EXISTS tasks (
  id INT(4) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(80),
  note VARCHAR(500),
  type_id INT(4) UNSIGNED NOT NULL,
  state_id  INT(4) UNSIGNED NOT NULL,
  priority_id INT(4) UNSIGNED NOT NULL,
  project_id INT(4) UNSIGNED NOT NULL,

  FOREIGN KEY (type_id) REFERENCES task_types(id),
  FOREIGN KEY (state_id) REFERENCES task_states(id),
  FOREIGN KEY (priority_id) REFERENCES task_priorities(id)
  FOREIGN KEY (project_id) REFERENCES projects(id)
) engine=InnoDB;
