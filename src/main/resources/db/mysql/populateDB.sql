INSERT IGNORE INTO users VALUES (1,'Petr','Novak','123','petr@novak.cz');
INSERT IGNORE INTO users VALUES (2,'Pista','Hofnagl','123','pista@hofnagl.cz');
INSERT IGNORE INTO users VALUES (3,'Joseph','Stalin','123','joseph@stalin.cz');

INSERT IGNORE INTO projects VALUES (1,'Zachrante verlyby','2017-06-15',NULL ,1);
INSERT IGNORE INTO projects VALUES (2,'Utopte krecky','2018-09-22',NULL ,3);

INSERT IGNORE INTO users_projects VALUES (1,1);
INSERT IGNORE INTO users_projects VALUES (2,1);
INSERT IGNORE INTO users_projects VALUES (3,2);
INSERT IGNORE INTO users_projects VALUES (2,2);

INSERT IGNORE INTO task_states VALUES (1,'New');
INSERT IGNORE INTO task_states VALUES (2,'In making');
INSERT IGNORE INTO task_states VALUES (3,'Completed');


INSERT IGNORE INTO task_types VALUES (1,'Develop');
INSERT IGNORE INTO task_types VALUES (2,'Error');
INSERT IGNORE INTO task_types VALUES (3,'Suggestion');

INSERT IGNORE INTO task_priorities VALUES (1,'Low');
INSERT IGNORE INTO task_priorities VALUES (2,'Medium');
INSERT IGNORE INTO task_priorities VALUES (3,'Top');

INSERT IGNORE INTO tasks VALUES (1,'Oprava vodni nadrze','Je treba opravit vodni nadrz na topeni',2 ,1, 3,2);
INSERT IGNORE INTO tasks VALUES (2,'Oprava vodni nadrze','Je treba opravit vodni nadrz na zachranu',2 ,1, 3,1);
