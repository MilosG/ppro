package org.springframework.samples.petclinic.model;

import javax.persistence.*;

@Entity
@Table(name = "tasks")
public class Task extends BaseEntity {
    private String name;

    private String note;

    @ManyToOne
    @JoinColumn(name = "type_id")
    private TaskType type;
    @ManyToOne
    @JoinColumn(name = "state_id")
    private TaskState state;
    @ManyToOne
    @JoinColumn(name = "priority_id")
    private TaskPriority priority;


    //GETTERS AND SETTERS

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public TaskType getType() {
        return type;
    }

    public void setType(TaskType type) {
        this.type = type;
    }

    public TaskState getState() {
        return state;
    }

    public void setState(TaskState state) {
        this.state = state;
    }

    public TaskPriority getPriority() {
        return priority;
    }

    public void setPriority(TaskPriority priority) {
        this.priority = priority;
    }
}
