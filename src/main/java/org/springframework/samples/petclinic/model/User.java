package org.springframework.samples.petclinic.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "users")
public class User extends BaseEntity {
    private String email;
    private String password;
    @Column(name = "first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;

    @ManyToMany(mappedBy = "developers")
    private Set<Project> projectsAsDeveloper;

    @OneToMany(mappedBy = "projectManager")
    private Set<Project> projectsAsOwner;



    //GETTERS AND SETTERS

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Project> getProjectsAsDeveloper() {
        return projectsAsDeveloper;
    }

    public void setProjectsAsDeveloper(Set<Project> projectsAsDeveloper) {
        this.projectsAsDeveloper = projectsAsDeveloper;
    }

    public Set<Project> getProjectsAsOwner() {
        return projectsAsOwner;
    }

    public void setProjectsAsOwner(Set<Project> projectsAsOwner) {
        this.projectsAsOwner = projectsAsOwner;
    }
}
